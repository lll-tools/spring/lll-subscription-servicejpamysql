package com.luislain.spring.subscription.servicejpamysql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SubscriptionExampleData {
    private static final Logger log = LoggerFactory.getLogger(SubscriptionExampleData.class);

    @Bean
    CommandLineRunner initDatabase(SubscriptionRepository repository) {

        return args -> {
            log.info("Preloading "
                    + repository.save(new Subscription("user10@domain.com", "User10", "Female", "1999/01/20", true)));
            log.info("Preloading "
                    + repository.save(new Subscription("user11@domain.com", "User11", "Male", "1999/01/21", true)));
            log.info("Preloading "
                    + repository.save(new Subscription("user12@domain.com", "User12", "Female", "1999/01/22", true)));
            log.info("Preloading "
                    + repository.save(new Subscription("user13@domain.com", "User13", "Male", "1999/01/23", true)));
            log.info("Preloading "
                    + repository.save(new Subscription("user14@domain.com", "User14", "Female", "1999/01/24", true)));
            log.info("Preloading "
                    + repository.save(new Subscription("user15@domain.com", "User15", "Male", "1999/01/25", true)));
        };
    }
}
