package com.luislain.spring.subscription.servicejpamysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicejpamysqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicejpamysqlApplication.class, args);
	}

}
