package com.luislain.spring.subscription.servicejpamysql;

import org.springframework.data.repository.CrudRepository;

public interface SubscriptionRepository extends CrudRepository<Subscription, Long> {
  
}
