package com.luislain.spring.subscription.servicejpamysql;

public class SubscriptionNotFoundException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    SubscriptionNotFoundException(Long id) {
        super("Subscription not found " + id);
    }
}
