package com.luislain.spring.subscription.servicejpamysql;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/api")
public class SubscriptionController {

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @GetMapping(path = "/add")
    public @ResponseBody String addSubscription(@RequestParam String email, @RequestParam String name,
            @RequestParam String gender, @RequestParam String dateofbirthString, @RequestParam String consentString) {
        Subscription n = new Subscription();
        n.setFirstName(name);
        n.setEmail(email);
        n.setGender(gender);
        n.setDateofbirthString(dateofbirthString);
        if (consentString.startsWith("T")) {
            n.setConsentString(true);
        } else {
            n.setConsentString(false);
        }
        subscriptionRepository.save(n);
        return "Saved";
    }

    @PostMapping(path = "/add")
    public @ResponseBody String updateSubscription(@RequestParam String name, @RequestParam String email) {
        Subscription n = new Subscription();
        n.setFirstName(name);
        n.setEmail(email);
        subscriptionRepository.save(n);
        return "Saved";
    }

    @GetMapping(path = "/cancel")
    public @ResponseBody String cancelSubscription(@RequestParam Long id) {
        subscriptionRepository.deleteById(id);
        return "Canceled";
    }

    @GetMapping(path = "/details")
    public @ResponseBody Optional<Subscription> addNewSubscription(@RequestParam Long id) {
        return subscriptionRepository.findById(id);
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<Subscription> getAllSubscriptions() {
        // This returns a JSON or XML
        return subscriptionRepository.findAll();
    }

}
